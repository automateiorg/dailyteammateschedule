import { Injectable } from '@angular/core';  
import { HttpClient, HttpErrorResponse } from '@angular/common/http';  
import { Observable } from 'rxjs';  
import { ScheduleVM } from '../Classes/schedule-vm';  
import { map } from "rxjs/operators";
@Injectable({  
  providedIn: 'root'  
})  
export class ScheduleService {  
  Url = 'http://scadevjobs.com/api';  
  constructor(private http:HttpClient) { }  
  GetSchedules(loc, dt): Observable<ScheduleVM[]>  
  {  
    return this.http.get(this.Url + '/Schedules/' + loc + '/' + dt).pipe(
      map((res: any) => {
        return res.data.map(item => {
          return new ScheduleVM(
              item.teammateName,
              item.teammateType,
              item.monday,
              item.tuesday,
              item.wednesday,
              item.thursday,
              item.friday,
              item.saturday,
              item.sunday
          );
        });
      })
    );
       
  }  
} 