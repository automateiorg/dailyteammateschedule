import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common'
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable({providedIn: 'root' })
export class ExcelService {
  pipe = new DatePipe('en-US');
  constructor() { }

  public exportAsExcelFile(json: any[], excelFileName: string, dateSelected: Date): void {
    debugger
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    console.log('worksheet',worksheet);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName, dateSelected);
  }

  private saveAsExcelFile(buffer: any, fileName: string, dateSelected: Date): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    var shortDate =this.pipe.transform(dateSelected, 'yyyyMMdd')
    FileSaver.saveAs(data, fileName + '_export_week_of_' + shortDate + EXCEL_EXTENSION);
  }

}