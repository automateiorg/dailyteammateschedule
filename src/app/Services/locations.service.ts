import { Injectable } from '@angular/core';  
import { HttpClient, HttpErrorResponse } from '@angular/common/http';  
import { Observable } from 'rxjs';  
import { LocationsVM } from '../Classes/locations-vm';  
import { map } from "rxjs/operators";
@Injectable({  
  providedIn: 'root'  
})  
export class LocationsService {  
  Url = 'http://scadevjobs.com/api';  
  constructor(private http:HttpClient) { }  
  LocationsDDL(): Observable<LocationsVM[]>  
  {  
    return this.http.get(this.Url + '/Locations').pipe(
      map((res: any) => {
        return res.data.map(item => {
          return new LocationsVM(
            item.facilityId,
            item.facilityName
          );
        });
      })
    );
       
  }  
}  