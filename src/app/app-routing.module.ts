import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SchedulegridComponent } from './components/schedulegrid/schedulegrid.component';
import { HelpComponent } from './components/help/help.component';

const routes: Routes = [
  {path: '', component: SchedulegridComponent},
  {path:'help', component:HelpComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
