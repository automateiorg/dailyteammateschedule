import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulegridComponent } from './schedulegrid.component';

describe('SchedulegridComponent', () => {
  let component: SchedulegridComponent;
  let fixture: ComponentFixture<SchedulegridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchedulegridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulegridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
