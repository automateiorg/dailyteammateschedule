import { Component, OnInit, ViewChild } from '@angular/core';  
import { FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule } from '@angular/forms'; 
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material'; 
import { Observable } from 'rxjs';  
import { LocationsVM } from 'src/app/Classes/locations-vm';  
import { LocationsService } from 'src/app/Services/locations.service'; 
import { ScheduleService } from 'src/app/Services/schedule.service';   
import { ExcelService } from 'src/app/Services/excel.service';
import { DatePipe } from '@angular/common'

@Component({
  selector: 'app-schedulegrid',
  templateUrl: './schedulegrid.component.html',
  styleUrls: ['./schedulegrid.component.sass', '../../app.component.sass']
})
export class SchedulegridComponent implements OnInit {

  formGroup: FormGroup;
  post: any = '';
  displayedColumns = ['teammateName', 'teammateType', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
  dataSource = new MatTableDataSource();
  postedDate = new Date();
  ansMessage : string = "Days without Anesthesia coverage: ";
  
  private _allSchedules: any;
  private _allLocations: Observable<LocationsVM[]>;
  private paginator :MatPaginator;
  private sort : MatSort;  
  @ViewChild(MatSort, null) set matSort(ms: MatSort) {
    this.sort = ms;
    this.setDataSourceAttributes();
}

@ViewChild(MatPaginator, null) set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setDataSourceAttributes();
}
  constructor(public datepipe: DatePipe,private formbuilder: FormBuilder,private LocationsService:LocationsService, private ScheduleService:ScheduleService, private ExcelService:ExcelService) { }

  ngOnInit() {
    this.createForm();
    this.FillLocationDDL(); 
  }
  NgAfterViewInit (){
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  
  setDataSourceAttributes() {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
  
      if (this.paginator && this.sort) {
          this.applyFilter('');
      }
  }
  FillLocationDDL()  
  {  
    this._allLocations=this.LocationsService.LocationsDDL();  
  }  

  GetSchedule(location, date){
    let formattedDate = this.datepipe.transform(date, 'yyyy-MM-dd');
    this._allSchedules = this.ScheduleService.GetSchedules(location, formattedDate).subscribe(  
      res => {  
        this.dataSource = new MatTableDataSource();  
        this.dataSource.data = res;  
        console.log(this.dataSource.data);  
        let mon = res.filter(obj => obj.teammateType == 'Anesthesia' && obj.monday != "OFF" ).length < 3;
        let tues =  res.filter(obj => obj.teammateType == 'Anesthesia' && obj.tuesday != "OFF" ).length < 3;
        let wed =  res.filter(obj => obj.teammateType == 'Anesthesia' && obj.wednesday != "OFF" ).length < 3;
        let thurs =  res.filter(obj => obj.teammateType == 'Anesthesia' && obj.thursday != "OFF" ).length < 3;
        let fri =  res.filter(obj => obj.teammateType == 'Anesthesia' && obj.friday != "OFF" ).length < 3;
        let sat = res.filter(obj => obj.teammateType == 'Anesthesia' && obj.saturday != "OFF" ).length < 3;
        if(mon){
          this.ansMessage = this.ansMessage.concat("Monday ");
        }
        if(tues){
          this.ansMessage = this.ansMessage.concat("Tuesday ");
        }
        if(wed){
          this.ansMessage = this.ansMessage.concat("Wednesday ");
        }
        if(thurs){
          this.ansMessage = this.ansMessage.concat("Thursday ");
        }
        if(fri){
          this.ansMessage =  this.ansMessage.concat("Friday ");
        }
        if(sat){
          this.ansMessage =  this.ansMessage.concat("Saturday ");
        }
      },  
      error => {  
        console.log('There was an error while retrieving data !!!' + error);  
      });  
      this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  
  dateFilter = (date: Date) => {
    var day = date.getDay();
    return day === 1;
  }
  createForm() {
    this.formGroup = this.formbuilder.group({
      'location': ['', [Validators.required]],
      'date': ['', Validators.required],
      
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  exportToExcel():void {
    this.ExcelService.exportAsExcelFile(this.dataSource.data, 'Schedule', this.post.date);
  }

    onSubmit(post) {
      this.GetSchedule(post.location, post.date);
      this.post = post;
      this.ansMessage = "Days without Anesthesia coverage: ";
    }

}
