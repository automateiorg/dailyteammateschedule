import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'solution-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass']
})
export class FooterComponent implements OnInit {
  year : any = new Date().getFullYear();
  title : string = "Daily Schedule"
  constructor() { }

  ngOnInit() {
  }

}